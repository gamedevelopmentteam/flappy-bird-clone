﻿using UnityEngine;
using System.Collections;

public class BackGround : MonoBehaviour {
	public float speed = 0.5f;
	public int numBGPanels = 6;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * speed * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.name == "BGLooper") {

			float widthOfBGObject = ((BoxCollider2D)collider2D).size.x - 0.015f;

			Vector3 pos = this.transform.position;

			pos.x += widthOfBGObject * numBGPanels;

			this.transform.position = pos;
		}
	}
}
